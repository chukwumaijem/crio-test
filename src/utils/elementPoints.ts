import IPoint from '../interfaces/IPoint';
import Polygon from "../models/elements/Polygon";

type minmaxPoints = {
  xMin?: number;
  xMax?: number;
  yMin?: number;
  yMax?: number;
};

export default class ElementPoints {

  /**
   * Get Min and Max values of both x and y for all elements
   *
   * @private
   * @param {points} minMaxXY
   * @returns {object}
   * @memberof ElementPoints
   */
  private static minMaxXY(points: IPoint[]): minmaxPoints {
    const xList: number[] = [];
    const yList: number[] = [];

    points.forEach(point => {
      xList.push(point.x);
      yList.push(point.y);
    });

    xList.sort((a, b) => a - b);
    yList.sort((a, b) => a - b);

    return {
      xMin: xList[0],
      xMax: xList.pop(),
      yMin: yList[0],
      yMax: yList.pop(),
    };
  }

  /**
   * Get Min and Max values of both x and y for all elements
   * 
   * keeping things real. :grin:
   * https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon
   * 
   * I'm makig the assumption that for this task we don't care about
   * polygons with holes.
   * 
   * @public
   * @param {Elements[]} findOverlappingElement
   * @returns {Elements}
   * @memberof ElementPoints
   */
  public static findOverlappingElement(polygons: Polygon[], clickX: number, clickY: number): Polygon | undefined {

    const element: Polygon | undefined = polygons.find(polygon => {
      const { xMin, xMax, yMin, yMax } = ElementPoints.minMaxXY(polygon.points);
      if (!xMin || !xMax || !yMin || !yMax) return false;

      if (clickX < xMin || clickX > xMax || clickY < yMin || clickY > yMax) {
        return false;
      }
      return true;
    });

    return element;
  }
}
