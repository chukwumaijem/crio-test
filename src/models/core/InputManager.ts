import _ from "lodash";
import Store from "../../store";
import ElementPoints from '../../utils/elementPoints';
export default class InputManager {

  private canvas: HTMLCanvasElement;
  private store: Store;

  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
    this.store = Store.getInstance();

    canvas.addEventListener("mousedown", event => this.handleMouseEvent(event));
    canvas.addEventListener("mouseup", event => this.handleMouseEvent(event));
    canvas.addEventListener("mousemove", event => this.handleMouseEvent(event));

    window.addEventListener("keydown", event => this.handleKeyboardEvent(event));
    window.addEventListener("keyup", event => this.handleKeyboardEvent(event));

    canvas.addEventListener("click", event => this.handleClickEvent(event));
  }


  private handleMouseEvent = (e: MouseEvent) => { }

  private handleKeyboardEvent = (e: KeyboardEvent) => { }

  private handleClickEvent = (e: MouseEvent) => {
    const elements = this.store.getElements();
    const clickX = e.clientX;
    const clickY = e.clientY;

    const element = ElementPoints.findOverlappingElement(elements, clickX, clickY);
    let renderProps = {};

    if (element ) {
      renderProps = { [element.id]: { border: { color: '#00f' }, close: true } };
    }
    const newState = Object.assign({}, _.cloneDeep(this.store.getState()), { renderProps })
    this.store.updateState(newState);
  }

}
